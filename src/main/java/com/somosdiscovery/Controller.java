package com.somosdiscovery;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.jsmpp.InvalidResponseException;
import org.jsmpp.PDUException;
import org.jsmpp.bean.BindType;
import org.jsmpp.bean.DataCodings;
import org.jsmpp.bean.ESMClass;
import org.jsmpp.bean.NumberingPlanIndicator;
import org.jsmpp.bean.RegisteredDelivery;
import org.jsmpp.bean.SMSCDeliveryReceipt;
import org.jsmpp.bean.TypeOfNumber;
import org.jsmpp.extra.NegativeResponseException;
import org.jsmpp.extra.ResponseTimeoutException;
import org.jsmpp.session.BindParameter;
import org.jsmpp.session.SMPPSession;
import org.jsmpp.util.AbsoluteTimeFormatter;
import org.jsmpp.util.TimeFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RestController;

@PropertySource("classpath:application.properties")
@RestController
public class Controller
{
	@Value("${coo.address}")
	public String URL;
	
	@Value("${coo.port}")
	public Integer port;
	
	@Value("${coo.amount}")
	public Integer amount;
	
	@Value("${coo.number}")
	public String number;
	
	public TimeFormatter timeFormatter = new AbsoluteTimeFormatter();
	
	@PostConstruct
	public void init()
	{
		sendAll();
	}
	
	public void sendAll()
	{
		try
		{
			Thread.sleep(1500);
			SMPPSession session = new SMPPSession();
			session.connectAndBind(URL, port, new BindParameter(BindType.BIND_TRX, "discover", "d!5c0v3r", "smpp", TypeOfNumber.INTERNATIONAL, NumberingPlanIndicator.ISDN, null));
			getMessages(amount).forEach(m -> 
			{
				send(session, m.getNumber(), m.getMessage());
			});
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	public void send(SMPPSession sess, String number, String message)
	{
		
		try
		{
			
			String messageId = sess.submitShortMessage("CMT",
				TypeOfNumber.INTERNATIONAL,
				NumberingPlanIndicator.ISDN,
				"<"+number+">",
				TypeOfNumber.INTERNATIONAL,
				NumberingPlanIndicator.ISDN,
				number,
				new ESMClass(),
				(byte)0,
				(byte)1,
				timeFormatter.format(new Date()),
				null,
				new RegisteredDelivery(SMSCDeliveryReceipt.SUCCESS_FAILURE),
				(byte)0,
				DataCodings.ZERO,
				(byte)0,
				message.getBytes());
			if (messageId!=null)
			{
				System.out.println("Message submitted, message_id is " + messageId);
			}
			else
			{
				System.out.println("Error en el envío");
			}
			/*
			 * you can save the submitted message to database.
			 */
			System.out.println(sess.getMessageReceiverListener());
			//Thread.sleep(2000);
		}
		catch (IOException e)
		{
			System.err.println("Failed connect and bind to host");
			e.printStackTrace();
		}
		catch (PDUException e)
		{
			// Invalid PDU parameter
			System.err.println("Invalid PDU parameter");
			e.printStackTrace();
		}
		catch (ResponseTimeoutException e)
		{
			// Response timeout
			System.err.println("Response timeout");
			e.printStackTrace();
		}
		catch (InvalidResponseException e)
		{
			// Invalid response
			System.err.println("Receive invalid respose");
			e.printStackTrace();
		}
		catch (NegativeResponseException e)
		{
			// Receiving negative response (non-zero command_status)
			System.err.println("Receive negative response");
			e.printStackTrace();
		}
    }
	
	public List<Message> getMessages(Integer messages)
	{
		List<Message> msg = new ArrayList<Message>();
		
		for (int i=1;i<=messages;i++)
		{
			Message m = new Message();
			m.setMessage("Mensaje de prueba "+i);
			m.setNumber(number);
			msg.add(m);
		}
		
		return msg;
	}
}
