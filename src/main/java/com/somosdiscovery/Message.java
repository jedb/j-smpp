package com.somosdiscovery;

public class Message
{
	public String number;
	public String message;
	
	public Message() {}
	
	public Message(String number, String message)
	{
		this.number=number;
		this.message=message;
	}
	
	public String getNumber()
	{
		return number;
	}
	
	public void setNumber(String number)
	{
		this.number = number;
	}
	
	public String getMessage()
	{
		return message;
	}
	
	public void setMessage(String message)
	{
		this.message = message;
	}
}